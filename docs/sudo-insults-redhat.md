1. Downalod sudo-*.el7.src.rpm from http://vault.centos.org/7.4.1708/os/Source/SPackages/
2. rpm2cpio ./sudo-*.el7.src.rpm | cpio -idmv
3. Open sudo.spec, and edit the config line

%configure \
        --prefix=%{_prefix} \
        --sbindir=%{_sbindir} \
        --libdir=%{_libdir} \
        --docdir=%{_datadir}/doc/%{name}-%{version} \
        --with-logging=syslog \
        --with-logfac=authpriv \
        --with-pam \
        --with-pam-login \
        --with-editor=/bin/vi \
        --with-env-editor \
        --with-gcrypt \
        --with-ignore-dot \
        --with-tty-tickets \
        --with-ldap \
        --with-ldap-conf-file="%{_sysconfdir}/sudo-ldap.conf" \
        --with-selinux \
        --with-passprompt="[sudo] password for %p: " \
        --with-linux-audit \
        --with-sssd \
        --with-insults=disabled \
        --with-all-insults
        
4. rpmbuild -v -bb --clean sudo.spec
5. yum install sudo-*.el7.src.rpm
