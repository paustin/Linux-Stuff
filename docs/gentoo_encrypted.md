# How to install Gentoo with FDE
This document will explain how to configure gentoo with FDE

## Getting ready
Sync Time
```
ntpd -q -g
```
Set password
```
passwd
```
Enable ssh
```
/etc/init.d/sshd start
```

## Preparing the disks
Trun on lvm support
```
/etc/init.d/lvm start
```
### Create partitions
```
parted -a optimal /dev/sda
unit mib
mklabel gpt

mkpart primary 1 3
name 1 grub
set 1 bios_grub on

mkpart primary fat32 3 515
name 2 boot
set 2 BOOT on

mkpart primary 515 8707
name 3 swap

mkpart primary 8707 -1
name 4 lvm 
set 4 lvm on
quit
```
### Activating the swap partition
```
mkswap /dev/sda3
swapon /dev/sda3
```
### Create boot filesystem
```
mkfs.vfat -F32 /dev/sda2
```
### Configure FDE Partition
#### Prepare encrypted partition
```
cryptsetup luksFormat -c aes-cbc-essiv:sha256 /dev/sda4
```
#### Create LVM inside encrypted block
```
cryptsetup luksOpen /dev/sda4 lvm
lvm pvcreate /dev/mapper/lvm 
vgcreate vg0 /dev/mapper/lvm 
lvcreate -l 100%FREE -n root vg0
```
#### Configure File Systems
```
mkfs.ext4 /dev/mapper/vg0-root
mount /dev/mapper/vg0-root /mnt/gentoo
```
## Installing stage3
### Downloading the stage tarball
```
cd /mnt/gentoo
wget http://gentoo.osuosl.org/releases/amd64/autobuilds/current-stage3-amd64/stage3-amd64-20180107T214502Z.tar.xz
tar xpvf stage3-*.tar.* --xattrs-include='*.*' --numeric-owner
```
### Configuring compile options
```
nano -w /mnt/gentoo/etc/portage/make.conf

# For T420
CFLAGS="-march=sandybridge -mtune=sandybridge -O2 -pipe"
CXXFLAGS="${CFLAGS}"
MAKEOPTS="-j5"
CPU_FLAGS_X86="aes avx mmx mmxext pclmul popcnt sse sse2 sse3 sse4_1 sse4_2 ssse3"
VIDEO_CARDS="intel i965"
```
## Installing base system
### Chrooting
#### Selecting mirrors
```
mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
mkdir --parents /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
```
#### Copy DNS info/Mounting the necessary filesystems
```
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
mount --types proc /proc /mnt/gentoo/proc 
mount --rbind /sys /mnt/gentoo/sys 
mount --make-rslave /mnt/gentoo/sys 
mount --rbind /dev /mnt/gentoo/dev 
mount --make-rslave /mnt/gentoo/dev 
```

```
test -L /dev/shm && rm /dev/shm && mkdir /dev/shm 
mount -t tmpfs -o nosuid,nodev,noexec shm /dev/shm
chmod 1777 /dev/shm 
```
#### Entering the new environment
```
chroot /mnt/gentoo /bin/bash 
source /etc/profile
export PS1="(chroot) $PS1"
mount /dev/sda2 /boot
```
### Configuring Portage
#### Installing an ebuild repository snapshot from the web
```
emerge-webrsync
```
#### Timezone
```
echo "America/Los_Angeles" > /etc/timezone
emerge --config sys-libs/timezone-data 
```
#### Configure locales
```
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
eselect locale list
eselect locale set 3
env-update && source /etc/profile && export PS1="(chroot) $PS1"
```
## Configuring the kernel
### Configure fstab
```
blkid >> /etc/fstab
nano /etc/fstab

# <fs>					<mountpoint>	<type>	<opts>			<dump/pass>
UUID="blkid{/dev/sda2}"			/boot		vfat	noauto,noatime		1 2
UUID="blkid{/dev/mapper/vg0-root}"	/		ext4	defaults		0 1
tmpfs					/tmp		tmpfs	size=4Gb		0 0
tmpfs					/run		tmpfs	size=100M		0 0
shm					/dev/shm	tmpfs	nodev,nosuid,noexec	0 0
```
Replace blkid{/dev/sda2} with the blkid of /dev/sda2 and blkid{/dev/mapper/vg0-root} with the blkid of /dev/mapper/vg0-root
### Configuring the Linux kernel
```
emerge --ask sys-kernel/gentoo-sources sys-kernel/genkernel sys-fs/cryptsetup sys-apps/pciutils
cd /usr/src/linux
make menuconfig
```
Kernel Config Defaults
```
General setup  --->
	[*] Initial RAM filesystem and RAM disk (initramfs/initrd) support
[*] Enable loadable module support
Device Drivers  --->
	Generic Driver Options --->
		[*] Maintain a devtmpfs filesystem to mount at /dev
		[ ]   Automount devtmpfs at /dev, after the kernel mounted the rootfs
	[*] Block Devices ---> 
		<*> Loopback device support 
	SCSI device support  --->
		<*> SCSI disk support
	Multiple devices driver support (RAID and LVM)  --->
		[*] Multiple devices driver support (RAID and LVM) --->
			<*> Device mapper support
			<*>   Crypt target support
		<*> Device mapper support
			<*> Crypt target support
			<*> Snapshot target
			<*> Mirror target
			<*> Multipath target
				<*> I/O Path Selector based on the number of in-flight I/Os
				<*> I/O Path Selector based on the service time
	HID support  --->
		-*- HID bus support
		<*>   Generic HID driver
		[*]   Battery level reporting for HID devices
		USB HID support  --->
			<*> USB HID transport layer
		[*] USB support  --->
			<*>     xHCI HCD (USB 3.0) support
			<*>     EHCI HCD (USB 2.0) support
			<*>     OHCI HCD (USB 1.1) support
	 <*> Multimedia support (NEW)
	 	[*]   Analog TV support
		[*]   Digital TV support
		[*]   AM/FM radio receivers/transmitters support
		[*]   Software defined radio support
		[*]   Remote Controller support
		[*]   Media Controller API
		[*]     Enable Media controller for DVB (EXPERIMENTAL)
		[*]   V4L2 sub-device userspace API
		[*]   DVB Network Support (NEW)
		[*]   Media USB Adapters  --->
			<*>   Conexant cx231xx USB video capture support
			[*]     Conexant cx231xx Remote Controller additional support
			<*>     Conexant Cx231xx ALSA audio module
			<*>     DVB/ATSC Support for Cx231xx based TV cards
	Graphics support  --->
		< > Nouveau (NVIDIA) cards 
Processor type and features  --->
	[*] Symmetric multi-processing support
File systems ---> 
	<*> Second extended fs support
	<*> The Extended 3 (ext3) filesystem
	<*> The Extended 4 (ext4) filesystem
	<*> Reiserfs support
	<*> JFS filesystem support
	<*> XFS filesystem support
	<*> Btrfs filesystem support
	<*> FUSE (Filesystem in Userspace) support
	DOS/FAT/NT Filesystems  --->
		<*> MSDOS fs support
		<*> VFAT (Windows-95) fs support
		<*> NTFS file system support
		[*]   NTFS write support 
	Pseudo Filesystems --->
		[*] /proc file system support
		[*] Tmpfs virtual memory file system support (former shm fs)
	[*] Network File Systems (NEW)  --->
[*] Cryptographic API --->
	<*> LRW support 
	<*> XTS support
	<*> RIPEMD-160 digest algorithm
	<*> SHA224 and SHA256 digest algorithm
	<*> SHA384 and SHA512 digest algorithms
	<*> Whirlpool digest algorithms 
	<*> AES cipher algorithms
	<*> AES cipher algorithms (x86_64)
	<*> Serpent cipher algorithm 
	<*> Twofish cipher algorithm
	<*> Twofish cipher algorithm (x86_64)
	<*> User-space interface for hash algorithms
	<*> User-space interface for symmetric key cipher algorithms 
[*] Virtualization (NEW)  --->
	<*>   Kernel-based Virtual Machine (KVM) support
	<*>     KVM for Intel processors support
```

```
make -j5 && make modules_install
make install
genkernel --luks --lvm --install initramfs

# For noobs who don't feel confident enough to compile a kernel
genkernel --luks --lvm --makeopts=-j5 all
```
### Installing firmware
```
emerge --ask sys-kernel/linux-firmware
```
## Networking information
### Host and domain information
```
nano -w /etc/conf.d/hostname
hostname="tux"
```
### Installing a DHCP client
```
emerge --ask net-misc/dhcpcd
```
### Optional: Install wireless networking tools
```
emerge --ask net-wireless/iw net-wireless/wpa_supplicant
```
## System information
### Root password
```
passwd
```
## Configuring the bootloader
```
emerge -a grub vim
vim /etc/default/grub
GRUB_CMDLINE_LINUX="dolvm crypt_root=UUID=blkid{/dev/sda4} root=/dev/mapper/vg0-root"
```
Replace blkid{/dev/sda4} with the blkid of /dev/sda4
```
mount /boot 
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg 
```
