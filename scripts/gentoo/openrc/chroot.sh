#!/bin/bash
source /etc/profile
export PS1="(chroot) $PS1"
set -e

SDA2UUID=$(blkid | grep /dev/sda2 | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')
SDA3UUID=$(blkid | grep /dev/sda3 | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')
SDA4UUID=$(blkid | grep /dev/sda4 | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')
VG0ROOTUUID=$(blkid | grep /dev/mapper/vg0-root | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')

mount /dev/sda2 /boot

#echo "CFLAGS='-march=westmere -mtune=westmere -O2 -pipe'" > /etc/portage/make.conf
echo "CFLAGS='-march=znver2 -mtune=znver2 -O2 -pipe'" > /etc/portage/make.conf
echo "CXXFLAGS='${CFLAGS}'" >> /etc/portage/make.conf
echo "MAKEOPTS='-j17'" >> /etc/portage/make.conf
echo "USE='zsh-completion offensive'" >> /etc/portage/make.conf
echo "VIDEO_CARDS='nvidia'" >> /etc/portage/make.conf

emerge-webrsync

echo "America/Los_Angeles" > /etc/timezone
emerge --config sys-libs/timezone-data 

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
eselect locale set en_US.utf8
env-update && source /etc/profile && export PS1="(chroot) $PS1"

echo "UUID='$SDA2UUID' /boot vfat noauto,noatime 1 2" > /etc/fstab
echo "UUID='$SDA3UUID' none swap sw 0 0" >> /etc/fstab
echo "UUID='$VG0ROOTUUID' / ext4 defaults 0 1" >> /etc/fstab

mkdir /etc/portage/package.license
echo "sys-kernel/linux-firmware linux-fw-redistributable no-source-code" > /etc/portage/package.license/linux-firmware
echo "sys-boot/grub mount" > /etc/portage/package.use/grub

emerge -uDU @world sys-kernel/gentoo-sources sys-kernel/genkernel sys-fs/cryptsetup sys-apps/pciutils sys-kernel/linux-firmware grub vim net-misc/dhcpcd sys-boot/os-prober app-shells/zsh

genkernel --save-config --install --luks --lvm --makeopts=-j17 --menuconfig all

echo "hostname='x201'" > /etc/conf.d/hostname

passwd

echo "GRUB_CMDLINE_LINUX='dolvm crypt_root=UUID=$SDA4UUID root=/dev/mapper/vg0-root'" >> /etc/default/grub

grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
