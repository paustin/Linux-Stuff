#!/bin/bash
# Script for compiling gentoo kernel

/etc/init.d/lvmetad start
make -j17
make modules
make modules_install
make install
genkernel --luks --lvm --install initramfs
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
emerge @module-rebuild
