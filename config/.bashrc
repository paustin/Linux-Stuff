# Put your fun stuff here.
export PATH=$PATH:~/bin

if [[ ${EUID} == 0 ]] ; then
  PS1='\[\033[01;31m\]\h\[\033[01;34m\] \w \$\[\033[00m\] '
else
  PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '
fi

alias ls='ls --color=auto'
alias ll="ls --color=auto -lh"
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'

# RC Clusters
alias coeus="ssh -o ServerAliveInterval=30 login2.coeus.rc.pdx.edu"
alias coeus-x="ssh -X -o ServerAliveInterval=30 login2.coeus.rc.pdx.edu"

alias gaia="ssh -o ServerAliveInterval=30 gaia.research.pdx.edu"
alias gaia-x="ssh -X -o ServerAliveInterval=30 gaia.research.pdx.edu"

# RC Standalone Linux Servers
alias circe="ssh -o ServerAliveInterval=30 circe.rc.pdx.edu"
alias circe-x="ssh -X -o ServerAliveInterval=30 circe.rc.pdx.edu"

alias hecate="ssh -o ServerAliveInterval=30 hecate.rc.pdx.edu"
alias hecate-x="ssh -X -o ServerAliveInterval=30 hecate.rc.pdx.edu"

alias agamede="ssh -o ServerAliveInterval=30 agamede.rc.pdx.edu"
alias agamede-x="ssh -X -o ServerAliveInterval=30 agamede.rc.pdx.edu"

alias hera="ssh -o ServerAliveInterval=30 hebe.rc.pdx.edu"
alias hebe="ssh -o ServerAliveInterval=30 hebe.rc.pdx.edu"
alias zelus="ssh -o ServerAliveInterval=30 zelus.rc.pdx.edu"

# Workstation
alias shuto="ssh -o ServerAliveInterval=30 shuto.oit.pdx.edu"
alias shuto-x="ssh -X -o ServerAliveInterval=30 shuto.oit.pdx.edu"

# OIT Servers
alias odin="ssh -o ServerAliveInterval=30 fenrir.oit.pdx.edu"
alias fenrir="ssh -o ServerAliveInterval=30 fenrir.oit.pdx.edu"

# CAT Servers
alias ada="ssh -o ServerAliveInterval=30 ada.cs.pdx.edu"
alias ada-x="ssh -X -o ServerAliveInterval=30 ada.cs.pdx.edu"

alias babbage="ssh -o ServerAliveInterval=30 babbage.cs.pdx.edu"
alias babbage-x="ssh -X -o ServerAliveInterval=30 babbage.cs.pdx.edu"

# Personal Servers
alias sly="ssh -o ServerAliveInterval=30 raccoonserver.net"
alias azeban="ssh -o ServerAliveInterval=30 raccoonserver.net"

# etc
alias date="date +'%a %b %d %r %Z %Y'"
alias justfuckmyshitup='sudo dtrace -w -n "BEGIN{ panic();}"'